package com.ssm.esb.api.common;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS)
public abstract class Message
{
    protected Message(){ }
}
