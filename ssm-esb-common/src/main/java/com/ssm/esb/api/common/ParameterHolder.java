package com.ssm.esb.api.common;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

public class ParameterHolder {
	private Map<String, String> parameters = new HashMap<String, String>();

	@JsonAnyGetter
	public Map<String, String> any() {
		return parameters;
	}

	@JsonAnySetter
	public void set(String name, String value) {
		parameters.put(name, value);
	}

	public Map<String, String> getParameters() {
		return parameters;
	}

}
