
package com.ssm.esb.api.common;

import java.lang.annotation.Retention;

import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface EnableValidation {
	public boolean enabled() default true;

}
