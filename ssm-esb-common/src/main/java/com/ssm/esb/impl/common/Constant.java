package com.ssm.esb.impl.common;

public class Constant {

	public static final String HEADER_USER = "userId";
	public static final String HEADER_SOURCE_SYSTEM_ID = "sourceSystemId";

	public static final int RESPONSE_CODE_ERROR = 9;
	public static final int RESPONSE_CODE_SUCCESS = 1;
}
