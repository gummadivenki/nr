package com.ssm.esb.impl.common.validator;

import java.util.Set;

import javax.validation.Configuration;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.component.bean.validator.HibernateValidationProviderResolver;

import com.ssm.esb.api.common.EnableValidation;
import com.ssm.esb.impl.common.Constant;
import com.ssm.esb.impl.common.ResponseWrapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

public class RequestValidator
	{

	private static Logger logger = LoggerFactory.getLogger(RequestValidator.class);

	private static ValidatorFactory factory;
	private static Validator validator;
	static{
		Configuration<?> configuration = Validation.byDefaultProvider().providerResolver(
			    new HibernateValidationProviderResolver()
			).configure();
		factory = configuration.buildValidatorFactory();
		validator = factory.getValidator();
	}

	// @Override
	public void validateMsg(Exchange exchange) throws Exception {
				logger.info("In here 0-1");

		Message in = exchange.getIn();

		logger.info("In here 0");
		logger.info("Boddy {}",ReflectionToStringBuilder.toString(in.getBody()));
		if(in == null || in.getBody() == null || in.getBody().toString().trim().equals("")){
			return;
		}
		System.out.println("In here 1");

		EnableValidation[] e = in.getBody().getClass().getAnnotationsByType(EnableValidation.class);
		if(e == null || e.length == 0 || e[0].enabled() == false){
			return;
		}
		logger.info("In here 2");
		Set<ConstraintViolation<Object>> errors = validator.validate(in.getBody());
		logger.info("Errors Size [" + errors.size() + "]");

		if(errors.size() > 0){
			ResponseWrapper response = new ResponseWrapper(Constant.RESPONSE_CODE_ERROR);
			for(ConstraintViolation<Object> constraint: errors){
				response.addMessages(constraint.getMessage());
			}
			exchange.getOut().setHeader("hasError", true);
			exchange.getOut().setBody(response);
		}
		logger.info("In here 3");

	}

}
