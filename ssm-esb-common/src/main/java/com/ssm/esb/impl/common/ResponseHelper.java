package com.ssm.esb.impl.common;

import java.io.IOException;

import org.apache.camel.Body;

public class ResponseHelper {

	public ResponseWrapper createSuccessResponse(@Body Object body) throws IOException{
		ResponseWrapper response = new ResponseWrapper(Constant.RESPONSE_CODE_SUCCESS);
		response.setBody(body);
		return response;
	}
	public ResponseWrapper createErrorResponse(@Body String msg) throws IOException{
		ResponseWrapper response = new ResponseWrapper(Constant.RESPONSE_CODE_ERROR);
		response.addMessages((String)msg);
		return response;
	}

}
