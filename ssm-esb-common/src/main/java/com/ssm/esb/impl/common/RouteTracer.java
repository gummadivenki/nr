package com.ssm.esb.impl.common;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.processor.DefaultExchangeFormatter;
import org.apache.camel.processor.DefaultExchangeFormatter.OutputStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RouteTracer implements Processor {

	private static final Logger logger = LoggerFactory.getLogger(RouteTracer.class);
	private DefaultExchangeFormatter format = new DefaultExchangeFormatter();
	{
		format.setMultiline(true);
		format.setSkipBodyLineSeparator(true);
	}
	@Override
	public void process(Exchange exchange) throws Exception {
		String MsgHist = org.apache.camel.util.MessageHelper.dumpMessageHistoryStacktrace(exchange, format, false);
        logger.info(MsgHist);
	}

}
