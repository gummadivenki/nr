package com.ssm.esb.impl.common.error;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.ext.ExceptionMapper;

import com.ssm.esb.impl.common.Constant;
import com.ssm.esb.impl.common.ResponseWrapper;

public class RestGenericExceptionMapper implements ExceptionMapper<Exception>{

	@Override
	public Response toResponse(Exception ex) {
		ResponseWrapper response = new ResponseWrapper(Constant.RESPONSE_CODE_ERROR);
		response.addMessages(ex.getMessage());
		ResponseBuilder rBuild = Response.ok(response, MediaType.APPLICATION_JSON)
				.status(Response.Status.INTERNAL_SERVER_ERROR);
		return rBuild.build();
	}

}
