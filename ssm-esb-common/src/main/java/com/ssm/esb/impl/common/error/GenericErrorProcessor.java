package com.ssm.esb.impl.common.error;

import com.ssm.esb.impl.common.Constant;
import com.ssm.esb.impl.common.ResponseWrapper;
import javax.servlet.ServletRequest;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.cxf.common.message.CxfConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GenericErrorProcessor implements Processor {

	private static final Logger logger = LoggerFactory.getLogger(GenericErrorProcessor.class);

	@Override
	public void process(Exchange exchange) throws Exception {
		Exception cause = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Exception.class);
		ResponseWrapper response = new ResponseWrapper(Constant.RESPONSE_CODE_ERROR);
		String sourceSystemId = exchange.getIn().getHeader("sourceSystemId", String.class);
		if(sourceSystemId == null){
			org.apache.cxf.message.Message cxfMessage
				= exchange.getIn().getHeader(CxfConstants.CAMEL_CXF_MESSAGE, org.apache.cxf.message.Message.class);
			if(cxfMessage != null){
				ServletRequest request = (ServletRequest) cxfMessage.get("HTTP.REQUEST");
				sourceSystemId = request.getRemoteAddr();
			}
		}
		logger.error("[{}] [{}] [{}] Unexpected error occurred:"
				, sourceSystemId
				, response.getTraceTimestamp()
				, response.getTraceId()
				, cause);
		exchange.getIn().setHeader("hasError", true);
		response.addMessages(cause.getMessage());
		exchange.getIn().setBody(response);
	}

}
