package com.ssm.esb.impl.common.error;

import org.apache.camel.builder.RouteBuilder;


public class ErrorHandlerRouteBuilder extends RouteBuilder{

	@Override
	public void configure() throws Exception {

		from("direct-vm:commonErrorHandler")
		.routeId("Common-ErrorHandlerRoute")
		.process(new GenericErrorProcessor())
		;
	}

}
