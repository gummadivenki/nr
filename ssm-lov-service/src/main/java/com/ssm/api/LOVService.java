package com.ssm.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ssm.datamodel.LookUpRqObject;




@Path("/")
public interface LOVService {
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	//@Path("/getLov/")
	public Response getLOV(LookUpRqObject lookUpRqObject);
}
