package 	 com.ssm.api;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;



@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)

public class LOVMessage {

	private String lookup_id;

	private String lookup_value;

	public  String getLookup_id()
	{
		return this.lookup_id;
	}

	public  void setLookup_id(String lookup_id)
	{
		this.lookup_id = lookup_id;
	}

	public  String getLookup_value()
	{
		return this.lookup_value;
	}


	public  void setLookup_value(String lookup_value)
	{
		this.lookup_value = lookup_value;
	}

}
