package com.ssm.datamodel;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * @author bujji
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="extraCurricularResponseObject")
@XmlType(name="extraCurricularResponseObject")
public class ExtraCurricularResponseObject {

	/*private String name;
	private String address;
	private String email;
	private String icNumber;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getIcNumber() {
		return icNumber;
	}
	public void setIcNumber(String icNumber) {
		this.icNumber = icNumber;
	}*/
	
	private String status;
	private String operation;
	private List<ExtraCurricularData> extraCurricularData;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public List<ExtraCurricularData> getExtraCurricularData() {
		return extraCurricularData;
	}
	public void setExtraCurricularData(List<ExtraCurricularData> extraCurricularData) {
		this.extraCurricularData = extraCurricularData;
	}
	
	
	
	
	
}
