package com.ssm.datamodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="citizenObject")

public class CitiZenReference {

	private String citizen_no;
	private String ic_no;
	private String dob;
	private String dod;
	private String oku_status;
	private String citizen_name;
	private String sex;
	private String identitytype_id;
	private String createdbydate;
	private String createdbyname;
	private String modifiedbydate;
	private String modifiedbyname;
	public String getCitizen_no() {
		return citizen_no;
	}
	public void setCitizen_no(String citizen_no) {
		this.citizen_no = citizen_no;
	}
	public String getIc_no() {
		return ic_no;
	}
	public void setIc_no(String ic_no) {
		this.ic_no = ic_no;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getDod() {
		return dod;
	}
	public void setDod(String dod) {
		this.dod = dod;
	}
	public String getOku_status() {
		return oku_status;
	}
	public void setOku_status(String oku_status) {
		this.oku_status = oku_status;
	}
	public String getCitizen_name() {
		return citizen_name;
	}
	public void setCitizen_name(String citizen_name) {
		this.citizen_name = citizen_name;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getIdentitytype_id() {
		return identitytype_id;
	}
	public void setIdentitytype_id(String identitytype_id) {
		this.identitytype_id = identitytype_id;
	}
	public String getCreatedbydate() {
		return createdbydate;
	}
	public void setCreatedbydate(String createdbydate) {
		this.createdbydate = createdbydate;
	}
	public String getCreatedbyname() {
		return createdbyname;
	}
	public void setCreatedbyname(String createdbyname) {
		this.createdbyname = createdbyname;
	}
	public String getModifiedbydate() {
		return modifiedbydate;
	}
	public void setModifiedbydate(String modifiedbydate) {
		this.modifiedbydate = modifiedbydate;
	}
	public String getModifiedbyname() {
		return modifiedbyname;
	}
	public void setModifiedbyname(String modifiedbyname) {
		this.modifiedbyname = modifiedbyname;
	}
	
	
	
	
	
}
