package com.ssm.datamodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * 
 * @author bujji
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="extraCurricularRequestObject")
@XmlType(name="extraCurricularRequestObject")
public class ExtraCurricularRequestObject {

private String sponsor_id;

public String getSponsor_id() {
	return sponsor_id;
}

public void setSponsor_id(String sponsor_id) {
	this.sponsor_id = sponsor_id;
}




}
