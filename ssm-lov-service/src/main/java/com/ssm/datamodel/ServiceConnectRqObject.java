/**
 * 
 */
package com.ssm.datamodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author leoanbarasanm
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="serviceConnectRqObject")

public class ServiceConnectRqObject {
	
	private Integer userId;

	/**
	 * @return the userId
	 */
	public Integer getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

}
