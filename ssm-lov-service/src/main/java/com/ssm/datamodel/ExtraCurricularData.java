package com.ssm.datamodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="extraCurricularData")
public class ExtraCurricularData {
	
	private String sponsor_id;
	private String sponsor_name;
	private String sponsor_email;
	private String sponsor_phone_number;
	private String sponsor_type_id;
	public String getSponsor_id() {
		return sponsor_id;
	}
	public void setSponsor_id(String sponsor_id) {
		this.sponsor_id = sponsor_id;
	}
	public String getSponsor_name() {
		return sponsor_name;
	}
	public void setSponsor_name(String sponsor_name) {
		this.sponsor_name = sponsor_name;
	}
	public String getSponsor_email() {
		return sponsor_email;
	}
	public void setSponsor_email(String sponsor_email) {
		this.sponsor_email = sponsor_email;
	}
	public String getSponsor_phone_number() {
		return sponsor_phone_number;
	}
	public void setSponsor_phone_number(String sponsor_phone_number) {
		this.sponsor_phone_number = sponsor_phone_number;
	}
	public String getSponsor_type_id() {
		return sponsor_type_id;
	}
	public void setSponsor_type_id(String sponsor_type_id) {
		this.sponsor_type_id = sponsor_type_id;
	}
	
	
	

}
