/**
 * 
 */
package com.ssm.datamodel;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author leoanbarasanm
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="serviceConnectRsObject")

public class ServiceConnectRsObject {
	
	private Boolean status;
	
	private ArrayList<ToDoObject> toDoObjects = new ArrayList<>();

	/**
	 * @return the status
	 */
	public Boolean getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Boolean status) {
		this.status = status;
	}

	/**
	 * @return the toDoObjects
	 */
	public ArrayList<ToDoObject> getToDoObjects() {
		return toDoObjects;
	}

	/**
	 * @param toDoObjects the toDoObjects to set
	 */
	public void setToDoObjects(ArrayList<ToDoObject> toDoObjects) {
		this.toDoObjects = toDoObjects;
	}
	
}
