/**
 * 
 */
package com.ssm.datamodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author leoanbarasanm
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="citizenLookUpRqObject")

public class CitizenLookUpRqObject {
	
	private String icNo;
	
	private String citizenName;

	/**
	 * @return the icNo
	 */
	public String getIcNo() {
		return icNo;
	}

	/**
	 * @param icNo the icNo to set
	 */
	public void setIcNo(String icNo) {
		this.icNo = icNo;
	}

	/**
	 * @return the citizenName
	 */
	public String getCitizenName() {
		return citizenName;
	}

	/**
	 * @param citizenName the citizenName to set
	 */
	public void setCitizenName(String citizenName) {
		this.citizenName = citizenName;
	}
	
}
