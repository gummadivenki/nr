package com.ssm.datamodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="okulookUpRsObject")

public class OKULookUpRsObject {

	private String icNo;
	private String OKUStatus;
	
	public String getIcNo() {
		return icNo;
	}
	public void setIcNo(String icNo) {
		this.icNo = icNo;
	}
	public String getOKUStatus() {
		return OKUStatus;
	}
	public void setOKUStatus(String oKUStatus) {
		OKUStatus = oKUStatus;
	}

}
