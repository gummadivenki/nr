package com.ssm.datamodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="examResultData")
public class ExamResultData {
	
	private String student_id;
	private String exam_id;
	private String subject_id;
	private String ic_no;
	private String results;
	private String is_deleted;
	private String created_by_name;
	private String created_by_date;
	private String modified_by_name;
	private String modified_by_date;
	public String getStudent_id() {
		return student_id;
	}
	public void setStudent_id(String student_id) {
		this.student_id = student_id;
	}
	public String getExam_id() {
		return exam_id;
	}
	public void setExam_id(String exam_id) {
		this.exam_id = exam_id;
	}
	public String getSubject_id() {
		return subject_id;
	}
	public void setSubject_id(String subject_id) {
		this.subject_id = subject_id;
	}
	public String getIc_no() {
		return ic_no;
	}
	public void setIc_no(String ic_no) {
		this.ic_no = ic_no;
	}
	public String getResults() {
		return results;
	}
	public void setResults(String results) {
		this.results = results;
	}
	public String getIs_deleted() {
		return is_deleted;
	}
	public void setIs_deleted(String is_deleted) {
		this.is_deleted = is_deleted;
	}
	public String getCreated_by_name() {
		return created_by_name;
	}
	public void setCreated_by_name(String created_by_name) {
		this.created_by_name = created_by_name;
	}
	public String getCreated_by_date() {
		return created_by_date;
	}
	public void setCreated_by_date(String created_by_date) {
		this.created_by_date = created_by_date;
	}
	public String getModified_by_name() {
		return modified_by_name;
	}
	public void setModified_by_name(String modified_by_name) {
		this.modified_by_name = modified_by_name;
	}
	public String getModified_by_date() {
		return modified_by_date;
	}
	public void setModified_by_date(String modified_by_date) {
		this.modified_by_date = modified_by_date;
	}
	
	
	

}
