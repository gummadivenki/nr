/**
 * 
 */
package com.ssm.datamodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author leoanbarasanm
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="examDetailLookUpRqObject")

public class ExamDetailLookUpRqObject {
	
	private Integer citizenNo;

	/**
	 * @return the citizenNo
	 */
	public Integer getCitizenNo() {
		return citizenNo;
	}

	/**
	 * @param citizenNo the citizenNo to set
	 */
	public void setCitizenNo(Integer citizenNo) {
		this.citizenNo = citizenNo;
	}
	
}
