/**
 * 
 */
package com.ssm.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.ssm.datamodel.ServiceConnectRqObject;
import com.ssm.datamodel.ServiceConnectRsObject;

/**
 * @author leoanbarasanm
 *
 */

@WebService (name = "ServiceConnectWS")

public interface ServiceConnectWS {

	@WebResult(name = "serviceConnectRsObject")
	@WebMethod(operationName = "serviceConnectRequest")
	public ServiceConnectRsObject serviceConnectRequest(@WebParam(name = "serviceConnectRqObject") ServiceConnectRqObject serviceConnectRqObject);
}
