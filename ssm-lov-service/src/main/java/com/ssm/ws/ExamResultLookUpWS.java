package com.ssm.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.ssm.datamodel.ExamResultLookUpRsObject;
import com.ssm.datamodel.ExamresultLookUprqObject;


@WebService( name = "ExamResultLookUpWS")


public interface ExamResultLookUpWS {
	@WebResult(name = "examResultsRsObject")
	@WebMethod(operationName = "examResultsRqObject")
    public ExamResultLookUpRsObject examresultlookupRequest(
    		@WebParam(name = "examResultsRqObject")
    		ExamresultLookUprqObject okulookupRqObject);
}	