/**
 * 
 */
package com.ssm.ws;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.component.cxf.common.message.CxfConstants;
import org.apache.cxf.message.MessageContentsList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ssm.datamodel.CitizenLookUpRqObject;

/**
 * @author leoanbarasanm
 *
 */
public class CitizenLookupProcessor implements Processor {

	private static Logger logger = LoggerFactory.getLogger(CitizenLookupProcessor.class);
	
	@Override
	public void process(Exchange exchange) throws Exception {
		Message inMessage = exchange.getIn();
		MessageContentsList msgListIn = (MessageContentsList)inMessage.getBody();
		CitizenLookUpRqObject response = (CitizenLookUpRqObject)msgListIn.get(0); 
		logger.info("IC No " + response.getIcNo());
		logger.info("Citizen Name " + response.getCitizenName());
		inMessage.setBody(response,CitizenLookUpRqObject.class);
		inMessage.setHeader(CxfConstants.OPERATION_NAME, "citizenLookupRequest");
		inMessage.setHeader(CxfConstants.OPERATION_NAMESPACE, "http://ws.ssm.com/");
	}

}
