package com.ssm.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.ssm.datamodel.LookUpRqObject;
import com.ssm.datamodel.LookUpRsObject;


@WebService( name = "LookUpWS")


public interface LookUpWS {
	@WebResult(name = "lookUpRsObject")
	@WebMethod(operationName = "lookupRequest")
    public LookUpRsObject lookupRequest(
    		@WebParam(name = "lookUpRqObject")
    		LookUpRqObject lookupRqObject);
}	