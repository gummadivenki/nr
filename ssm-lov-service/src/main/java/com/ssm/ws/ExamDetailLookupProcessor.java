/**
 * 
 */
package com.ssm.ws;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.component.cxf.common.message.CxfConstants;
import org.apache.cxf.message.MessageContentsList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ssm.datamodel.CitizenLookUpRsObject;
import com.ssm.datamodel.ExamDetailLookUpRqObject;


/**
 * @author leoanbarasanm
 *
 */
public class ExamDetailLookupProcessor implements Processor {

	private static Logger logger = LoggerFactory.getLogger(ExamDetailLookupProcessor.class);
	
	@Override
	public void process(Exchange exchange) throws Exception {
		Message inMessage = exchange.getIn();
		MessageContentsList msgListIn = (MessageContentsList)inMessage.getBody();
		CitizenLookUpRsObject response = (CitizenLookUpRsObject)msgListIn.get(0); 
		ExamDetailLookUpRqObject request = new ExamDetailLookUpRqObject();
		request.setCitizenNo(response.getCitizenNo());
		logger.info("Citizen No " + response.getCitizenNo());
		inMessage.setBody(request,ExamDetailLookUpRqObject.class);
		inMessage.setHeader(CxfConstants.OPERATION_NAME, "examDetailLookupRequest");
		inMessage.setHeader(CxfConstants.OPERATION_NAMESPACE, "http://ws.ssm.com/");
	}

}
