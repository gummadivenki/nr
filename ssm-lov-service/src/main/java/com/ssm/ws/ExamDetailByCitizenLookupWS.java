/**
 * 
 */
package com.ssm.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.ssm.datamodel.CitizenLookUpRqObject;
import com.ssm.datamodel.ExamDetailLookUpRsObject;

/**
 * @author leoanbarasanm
 *
 */

@WebService (name = "ExamDetailByCitizenLookupWS")

public interface ExamDetailByCitizenLookupWS {

	@WebResult(name = "examDetailLookUpRsObject")
	@WebMethod(operationName = "examDetailByCitizenLookupRequest")
	public ExamDetailLookUpRsObject examDetailByCitizenLookupRequest(@WebParam(name = "citizenLookUpRqObject") CitizenLookUpRqObject citizenLookUpRqObject);
}