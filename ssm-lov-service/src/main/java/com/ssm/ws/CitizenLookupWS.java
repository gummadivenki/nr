/**
 * 
 */
package com.ssm.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.ssm.datamodel.CitizenLookUpRqObject;
import com.ssm.datamodel.CitizenLookUpRsObject;

/**
 * @author leoanbarasanm
 *
 */

@WebService (name = "CitizenLookupWS")

public interface CitizenLookupWS {

	@WebResult(name = "citizenLookUpRsObject")
	@WebMethod(operationName = "citizenLookupRequest")
	public CitizenLookUpRsObject citizenLookupRequest(@WebParam(name = "citizenLookUpRqObject") CitizenLookUpRqObject citizenLookUpRqObject);
}
