package com.ssm.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.ssm.datamodel.OKULookUpRqObject;
import com.ssm.datamodel.OKULookUpRsObject;


@WebService( name = "OKULookUpWS")


public interface OKULookUpWS {
	@WebResult(name = "okulookUpRsObject")
	@WebMethod(operationName = "okulookupRequest")
    public OKULookUpRsObject okulookupRequest(
    		@WebParam(name = "okulookUpRqObject")
    		OKULookUpRqObject okulookupRqObject);
}	