/**
 * 
 */
package com.ssm.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.ssm.datamodel.CitizenLookUpRqObject;
import com.ssm.datamodel.CitizenLookUpRsObject;
import com.ssm.datamodel.ExamDetailLookUpRqObject;
import com.ssm.datamodel.ExamDetailLookUpRsObject;

/**
 * @author leoanbarasanm
 *
 */

@WebService (name = "ExamDetailLookupWS")

public interface ExamDetailLookupWS {

	@WebResult(name = "examDetailLookUpRsObject")
	@WebMethod(operationName = "examDetailLookupRequest")
	public ExamDetailLookUpRsObject examDetailLookupRequest(@WebParam(name = "examDetailLookUpRqObject") ExamDetailLookUpRqObject examDetailLookUpRqObject);
}
