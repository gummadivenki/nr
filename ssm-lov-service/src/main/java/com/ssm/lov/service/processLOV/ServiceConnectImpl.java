/**
 * 
 */
package com.ssm.lov.service.processLOV;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.apache.camel.Exchange;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ssm.datamodel.ServiceConnectRqObject;
import com.ssm.datamodel.ServiceConnectRsObject;
import com.ssm.datamodel.ToDoObject;

/**
 * @author leoanbarasanm
 *
 */
public class ServiceConnectImpl {

	private static Logger logger = LoggerFactory.getLogger(ServiceConnectImpl.class);
	
	public ServiceConnectRsObject getToDO(ServiceConnectRqObject bodyContent, Exchange exchange) {
		ServiceConnectRsObject result = new ServiceConnectRsObject();
		result.setStatus(false);
		
		try {
			URL url = new URL("https://jsonplaceholder.typicode.com/todos?userId=" + bodyContent.getUserId());
			HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
			httpCon.setDoOutput(true);
			httpCon.setRequestMethod("GET");
			httpCon.setRequestProperty("Content-Type","application/json");
			
			BufferedReader br;
			if (200 <= httpCon.getResponseCode() && httpCon.getResponseCode() <= 299) {
				br = new BufferedReader(new InputStreamReader((httpCon.getInputStream())));
				StringBuilder sb = new StringBuilder();
				String output;
				while ((output = br.readLine()) != null) {
					sb.append(output);
				}
				
				JSONArray jsonArray = new JSONArray(sb.toString());
				if (jsonArray.length() > 0) {
					
					ArrayList<ToDoObject> toDoObjects = new ArrayList<>();
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject object = jsonArray.getJSONObject(i);
						ToDoObject todo = new ToDoObject();
						todo.setId(object.getInt("id"));
						todo.setUserId(object.getInt("userId"));
						todo.setTitle(object.getString("title"));
						todo.setStatus(object.getBoolean("completed"));
						toDoObjects.add(todo);
					}
					result.setStatus(true);
					result.setToDoObjects(toDoObjects);
				}
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return result;
	}
}
