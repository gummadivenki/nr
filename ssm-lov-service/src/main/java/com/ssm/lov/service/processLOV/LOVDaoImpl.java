package com.ssm.lov.service.processLOV;

import javax.sql.DataSource;

import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.ssm.datamodel.CitizenLookUpRqObject;
import com.ssm.datamodel.CitizenLookUpRsObject;
import com.ssm.datamodel.ExamDetailLookUpRqObject;
import com.ssm.datamodel.ExamDetailLookUpRsObject;
import com.ssm.datamodel.ExamResultLookUpRsObject;
import com.ssm.datamodel.ExamresultLookUprqObject;
import com.ssm.datamodel.ExtraCurricularRequestObject;
import com.ssm.datamodel.ExtraCurricularResponseObject;
import com.ssm.datamodel.LookUpRqObject;
import com.ssm.datamodel.LookUpRsObject;
import com.ssm.datamodel.OKULookUpRqObject;
import com.ssm.datamodel.OKULookUpRsObject;

public class LOVDaoImpl {
	
	private static Logger logger = LoggerFactory.getLogger(LOVDaoImpl.class);
	private JdbcTemplate jdbcTemplate;
	
	public void setDataSource( DataSource dataSource ) 
	{
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	public LookUpRsObject searchLOV(LookUpRqObject bodyContent, Exchange exchange)
	{
		String  sqlString = "select lookup_id, lookup_value from SSM_LOOKUP_TABLE where lookup_id = '" + bodyContent.getIcNo() + "'" ;

		Object result = jdbcTemplate.queryForObject(sqlString, 
				 new LOVResultMapper() );
		if (result!=null && result instanceof LookUpRsObject)
		{
			((LookUpRsObject)result).setIcNo(bodyContent.getIcNo());	
			return (LookUpRsObject)result;
		}
		else
		{
			return null;
		}
	}
	
	@SuppressWarnings("finally")
	public OKULookUpRsObject searchOKU(OKULookUpRqObject bodyContent, Exchange exchange)
	{
		
		String  sqlString = "select oku_status as status from rp.citizen_info where ic_no = ?" ;
		
		OKULookUpRsObject res = null;
		
		try {
			logger.info(":::::::ICNumber::::::::::>"+bodyContent.getIcNo());
			res = (OKULookUpRsObject) jdbcTemplate.queryForObject(
					sqlString, new Object[] { bodyContent.getIcNo().trim() }, new LOVResultMapper());
			logger.info(":::::::ICNumber End::::::::::>"+res);
			
		} catch (EmptyResultDataAccessException e) {
			logger.info("::::::Exception Got:::::::::"+e.getMessage());
			res = new OKULookUpRsObject();
		} finally {
			return res;
		}
		
		
		/*String sql = "SELECT * FROM rp.citizen_reference";

		List<CitiZenReference> citizenList = new ArrayList<CitiZenReference>();
		logger.info("::::::Connection:::::::"+jdbcTemplate);
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		OKULookUpRsObject res = new OKULookUpRsObject();
		if(rows != null){
			for (Map row : rows) {
				logger.info(":::::::Rows::"+row);
				res.setStatus("SUCCESS");
				res.setOperation("GET_CITIZEN_INFO");
				CitiZenReference citizen = new CitiZenReference();
				citizen.setIc_no(String.valueOf(row.get("IC_NO")));
				citizen.setOku_status(String.valueOf(row.get("OKU_STATUS")));
				citizen.setCitizen_name(String.valueOf(row.get("CITIZEN_NAME")));
				citizen.setCitizen_no(String.valueOf(row.get("CITIZEN_NO")));
				citizen.setDob((String)(row.get("DATE_OF_BIRTH")));
				citizen.setDod((String)(row.get("DATE_OF_DEATH")));
				citizen.setIdentitytype_id(String.valueOf(row.get("IDENTITY_TYPE_ID")));
				
				citizenList.add(citizen);
			}
		}else{
			logger.info("Data can't found.........");
			res.setStatus("FAIL-NO Data Found......");
			res.setOperation("GET_CITIZEN_INFO");
			res.setCitizenList(citizenList);
		}
		
		
		res.setCitizenList(citizenList);
		return res;*/
		
		
		
		/*String sql = "select * from rp.citizen_reference where ic_no ="+bodyContent.getIcNo().trim();

		List<OKULookUpRsObject> okuList = new ArrayList<OKULookUpRsObject>();

		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		for (Map row : rows) {
			logger.info("::::::::::Rows::::::::::"+rows);
			OKULookUpRsObject oku = new OKULookUpRsObject();
			oku.setIcNo((String)(row.get("IC_NO")));
			oku.setOKUStatus((String)(row.get("OKU_STATUS")));
			okuList.add(oku);
		}
		
		return okuList;*/

		
		
		
	}
	@SuppressWarnings("finally")
	public ExamResultLookUpRsObject searchExamResult(ExamresultLookUprqObject bodyContent, Exchange exchange)
	{
		
		String  sqlString = "select student_id as student_id,exam_id as exam_id,subject_id as subject_id,results as results,ic_no as ic_no"
				+ " from rpn.exam_results where ic_no = ?" ;
		
		ExamResultLookUpRsObject res = null;
		
		try {
			res = (ExamResultLookUpRsObject) jdbcTemplate.queryForObject(
					sqlString, new Object[] { bodyContent.getIcNo() }, new ExamResultMapper());
			
		} catch (EmptyResultDataAccessException e) {
			res = new ExamResultLookUpRsObject();
		} finally {
			return res;
		}
		
		
		
	}
	
	
	/**
	 * Done by Venki on 17-03-2017
	 * @param bodyContent
	 * @param exchange
	 * @return
	 */
	@SuppressWarnings("finally")
	public ExtraCurricularResponseObject curricularDetails(ExtraCurricularRequestObject bodyContent, Exchange exchange)
	{
		
		String  sqlString = "select sponsor_id as sponsor_id,sponsor_name as sponsor_name,sponsor_email as sponsor_email,"
				+ " sponsor_phone_number as sponsor_phone_number,sponsor_type_id as sponsor_type_id from rp.sponsorship_providers where sponsor_id = ?" ;
		
		/*String  sqlString = "select sponsor_id as sponsor_id,sponsor_email as sponsor_email"
				+ "  from rp.sponsorship_providers where sponsor_id = ?" ;
		*/
		ExtraCurricularResponseObject res = null;
		logger.info("SponsorID::::::::::"+bodyContent.getSponsor_id());
		try {
			res = (ExtraCurricularResponseObject) jdbcTemplate.queryForObject(
					sqlString, new Object[] { bodyContent.getSponsor_id() }, new ExtraCurricularDetailsMapper());
			
		} catch (EmptyResultDataAccessException e) {
			res = new ExtraCurricularResponseObject();
		} finally {
			return res;
		}
	}
	
	@SuppressWarnings("finally")
	public CitizenLookUpRsObject searchCitizen(CitizenLookUpRqObject bodyContent, Exchange exchange) {
		
		CitizenLookUpRsObject result = new CitizenLookUpRsObject();
		try {
			String query = "SELECT citizen_no FROM rp.citizen_info where rp.citizen_info.ic_no = ? AND rp.citizen_info.citizen_name = ?";
			Object object = jdbcTemplate.queryForObject(query, new Object[] {bodyContent.getIcNo().trim(), bodyContent.getCitizenName().trim()}, new CitizenResultMapper());
			if (object != null && result instanceof CitizenLookUpRsObject) {
				result = (CitizenLookUpRsObject) object;
			} else {
				result.setStatus(false);
				result.setMessage("Error in citizen lookup!!!");
			}
		} catch (EmptyResultDataAccessException e) {
			result.setStatus(false);
			result.setMessage("Ic number and citizen name is not matched with the availble data!!!");
		} catch (Exception e) {
			result.setStatus(false);
			result.setMessage("Error in citizen lookup!!!");
			logger.info("::::::Exception Got:::::::::"+e.getMessage());
		} finally {
			return result;
		}
	}
	
	@SuppressWarnings("finally")
	public ExamDetailLookUpRsObject searchExamDetail(ExamDetailLookUpRqObject bodyContent, Exchange exchange) {
		
		ExamDetailLookUpRsObject result = new ExamDetailLookUpRsObject();
		try {
			String query = "SELECT ae.student_id, ae.exam_id, ae.subject_id, ae.course_id, ae.exam_type, ae.exam_name, ae.exam_std, ae.exam_code, ae.exam_year, ae.exam_result, ae.grade, ae.score "
					+ "FROM rpn_test.ac_exam AS ae LEFT JOIN rpn_test.res_mas_resident AS mr ON (ae.resident_id = mr.resident_id) WHERE mr.citizen_no = ?";
			Object object = jdbcTemplate.queryForObject(query, new Object[] {bodyContent.getCitizenNo()}, new ExamDetailResultMapper());
			if (object != null && result instanceof ExamDetailLookUpRsObject) {
				result = (ExamDetailLookUpRsObject) object;
			} else {
				result.setStatus(false);
				result.setMessage("Error in exam result lookup!!!");
			}
		} catch (EmptyResultDataAccessException e) {
			result.setStatus(false);
			result.setMessage("Exam detail is not found for the given citizen!!!");
		} catch (Exception e) {
			result.setStatus(false);
			result.setMessage("Error in exam result lookup!!!");
			logger.info("::::::Exception Got:::::::::"+e.getMessage());
		} finally {
			return result;
		}
	}
	
	
	public ExamDetailLookUpRsObject searchExamDetailByCitizen(CitizenLookUpRqObject bodyContent, Exchange exchange) {
		
		ExamDetailLookUpRsObject result = new ExamDetailLookUpRsObject();
		CitizenLookUpRsObject citizen = searchCitizen(bodyContent, exchange);
		if (citizen.getStatus()) {
			ExamDetailLookUpRqObject request = new ExamDetailLookUpRqObject();
			request.setCitizenNo(citizen.getCitizenNo());
			result = searchExamDetail(request, exchange);
		} else {
			result.setStatus(false);
			result.setMessage(citizen.getMessage());
		}
		
		
		
		return result;
	}
	
	
}



