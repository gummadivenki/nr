package com.ssm.lov.service.processLOV;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import com.ssm.datamodel.OKULookUpRsObject;

public class LOVResultMapper implements RowMapper<OKULookUpRsObject> {
	private static Logger logger = LoggerFactory.getLogger(LOVResultMapper.class);
	//Auto generated
	@Override
	public OKULookUpRsObject mapRow(ResultSet rs, int idx) throws SQLException {
			OKULookUpRsObject okulookUpRsObject = new OKULookUpRsObject();
		
			okulookUpRsObject.setOKUStatus((rs.getString("status")));
			logger.info("STATUS::::::::::::::::::>"+(rs.getString("status")));
			return okulookUpRsObject;
		
	}

}
