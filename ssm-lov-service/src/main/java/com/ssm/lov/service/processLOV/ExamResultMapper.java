package com.ssm.lov.service.processLOV;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.ssm.datamodel.ExamResultData;
import com.ssm.datamodel.ExamResultLookUpRsObject;

public class ExamResultMapper implements RowMapper<ExamResultLookUpRsObject> {

	//Auto generated
	@Override
	public ExamResultLookUpRsObject mapRow(ResultSet rs, int idx) throws SQLException {
			ExamResultLookUpRsObject okulookUpRsObject = new ExamResultLookUpRsObject();
			List<ExamResultData> examResult = new ArrayList<ExamResultData>();
			/*okulookUpRsObject.setSubjectId((rs.getString("subjectId")));
			okulookUpRsObject.setStudentId((rs.getString("studentId")));
			okulookUpRsObject.setExamId((rs.getString("examid")));
			okulookUpRsObject.setResults((rs.getString("results")));*/
			if(rs != null){
				okulookUpRsObject.setStatus("SUCCESS");
				okulookUpRsObject.setOperation("GET_EXAM_RESULTS");
				ExamResultData examResultvo = new ExamResultData();
				examResultvo.setExam_id((rs.getString("exam_id")));
				examResultvo.setIc_no((rs.getString("ic_no")));
				examResultvo.setResults((rs.getString("results")));
				examResultvo.setStudent_id((rs.getString("student_id")));
				examResultvo.setSubject_id((rs.getString("subject_id")));
				/*examResultvo.setCreated_by_name((rs.getString("created_by_name")));
				examResultvo.setModified_by_name((rs.getString("modified_by_name")));*/
				examResult.add(examResultvo);
				okulookUpRsObject.setExamResults(examResult);
				
			}else{
				okulookUpRsObject.setStatus("FAIL-Data Can't Found...");
				okulookUpRsObject.setOperation("GET_EXAM_RESULTS");
				okulookUpRsObject.setExamResults(examResult);
			}
		
		
			return okulookUpRsObject;
		
	}

}
