package com.ssm.lov.service.processLOV;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ssm.datamodel.LookUpRqObject;
import com.ssm.datamodel.LookUpRsObject;

public class ProcessMessage {
	private static Logger logger = LoggerFactory.getLogger(ProcessMessage.class);
	private static final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
	
	public LookUpRsObject processMessage(LookUpRqObject bodyContent, Exchange exchange) {
		String lovId = "E" + dateFormat.format(LocalDateTime.now());

	    LookUpRsObject res = new LookUpRsObject();
	    
	    String icNo = bodyContent.getIcNo();
	    if (icNo ==null || icNo.isEmpty()){
	    	return res;
	    } else if (icNo.startsWith("7")){
	    	res.setName("Jamalkari");
	    	res.setIcNo(icNo);
		    res.setAddress("456 Censof, Jalan Uni5, CyberJaya 51000, KL Malaysia");
		    res.setAge("39");
		    res.setDOB("01-10-1972");
		    res.setOKUType("A");
		    res.setSex("M");
	    } else if (icNo.startsWith("8")){
	    	res.setName("Jean Badani");
	    	res.setIcNo(icNo);
		    res.setAddress("12F Menara ABC, Jalan Seminar 1, Kuching 38900, Sarawak Malaysia");
		    res.setAge("29");
		    res.setDOB("01-10-1982");
		    res.setOKUType("A");
		    res.setSex("F");
	    } else if (icNo.startsWith("9")){
	    	res.setName("Ahmad Jeri");
	    	res.setIcNo(icNo);
		    res.setAddress("No. 567, Jalan Cincarok, Penang 32020, Penang Malaysia");
		    res.setAge("19");
		    res.setDOB("01-10-1992");
		    res.setOKUType("A");
		    res.setSex("M");
	    } else {
	    	res.setName("Serumi Jakik");
	    	res.setIcNo(icNo);
		    res.setAddress("No. 31, Taman Aseman, Puchong 49020, Selangor Malaysia");
		    res.setAge("45");
		    res.setDOB("01-10-1972");
		    res.setOKUType("A");
		    res.setSex("F");
	    }
	    
	    return res;
    }
}
