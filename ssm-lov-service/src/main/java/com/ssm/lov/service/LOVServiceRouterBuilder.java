package com.ssm.lov.service;

import org.apache.camel.builder.RouteBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ssm.lov.service.processLOV.CitizenLookUpMessage;
import com.ssm.lov.service.processLOV.ExamDetailLookUpMessage;
import com.ssm.lov.service.processLOV.ExtraCurricularDetailMessage;
import com.ssm.lov.service.processLOV.ProcessExamResultMessage;
import com.ssm.lov.service.processLOV.ProcessMessage;
import com.ssm.lov.service.processLOV.ProcessOKUMessage;
import com.ssm.lov.service.processLOV.ServiceConnectMessage;


public class LOVServiceRouterBuilder extends RouteBuilder {
	private static Logger logger = LoggerFactory.getLogger(LOVServiceRouterBuilder.class);

	private String inEndpoint;

	void setLovEndpoint(String uri) {
		this.inEndpoint=uri;
	}

	public LOVServiceRouterBuilder() {}

	public LOVServiceRouterBuilder(String inEndpoint) {
		this.inEndpoint = inEndpoint;
	}

	@Override
	public void configure() throws Exception {

		from("cxf:bean:ssm-lookup-endpoint")
		.routeId("LOV-WSEndPoint")
		.log("Start - Route for getting bio data information")
		.bean(ProcessMessage.class)
		.log("Body : [${body}]")
		.log("End - Route for getting bio data information");
		
		from("cxf:bean:ssm-okulookup-endpoint")
		.routeId("LOV-OKUWSEndPoint")
		.log("Start - Route for getting OKU status")
		.bean(ProcessOKUMessage.class)
		.to("bean:lovDao?method=searchOKU")
		.log("Body : [${body}]")
		.log("End - Route for getting OKU status");
		
		from("cxf:bean:ssm-serviceconnect-endpoint")
		.routeId("LOV-ServiceConnectWSEndPoint")
		.log("Start - Route for connecting service")
		.bean(ServiceConnectMessage.class)
		.to("bean:scDao?method=getToDO")
		.log("Body : [${body}]")
		.log("End - Route for connecting service");
		
		from("cxf:bean:mampu-examlookup-endpoint")
		.routeId("LOV-EXAMRESULTWSEndPoint")
		.log("Start - Route for getting Exam Result")
		.bean(ProcessExamResultMessage.class)
		.to("bean:lovDao?method=searchExamResult")
		.log("Body : [${body}]")
		.log("End - Route for getting  Exam Result");
		
		
		/**
		 * Circullar Details Route Configuration
		 */
		from("cxf:bean:mampu-curricular-endpoint")
		.routeId("Extra Curricullar Detail Service")
		.log("Start - Route for getting Curricular Result")
		.bean(ExtraCurricularDetailMessage.class)
		.to("bean:lovDao?method=curricularDetails")
		.log("Body : [${body}]")
		.log("End - Route for getting  curricular Result");
		
		from("cxf:bean:ssm-citizenlookup-endpoint")
		.routeId("LOV-CitizenLookupWSEndPoint")
		.log("Start - Route for Citizen Lookup")
		.bean(CitizenLookUpMessage.class)
		.to("bean:lovDao?method=searchCitizen")
		.log("Body : [${body}]")
		.log("End - Route for Citizen Lookup");
		
		from("cxf:bean:ssm-examdetaillookup-endpoint")
		.routeId("LOV-ExamDetailLookupWSEndPoint")
		.log("Start - Route for Exam Detail Lookup")
		.bean(ExamDetailLookUpMessage.class)
		.to("bean:lovDao?method=searchExamDetail")
		.log("Body : [${body}]")
		.log("End - Route for Exam Detail Lookup");

//		from("cxf:bean:ssm-examdetailfromcitizenlookup-endpoint")
//		.routeId("LOV-ExamDetailByCitizenLookupWSEndPoint")
//		.log("Start - Route for Exam Detail By Citizen Lookup")
//		.bean(CitizenLookUpMessage.class)
//		.to("bean:lovDao?method=searchExamDetailByCitizen")
//		.log("Body : [${body}]")
//		.log("End - Route for Exam Detail By Citizen Lookup");
	}

}

