package com.ssm.lov.service.processLOV;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.ssm.datamodel.ExamDetailLookUpRsObject;

public class ExamDetailResultMapper implements RowMapper<ExamDetailLookUpRsObject> {

	@Override
	public ExamDetailLookUpRsObject mapRow(ResultSet rs, int idx) throws SQLException {
		ExamDetailLookUpRsObject examDetailLookUpRsObject = new ExamDetailLookUpRsObject();
		if(rs != null){
			examDetailLookUpRsObject.setStatus(true);
			examDetailLookUpRsObject.setStudentId(rs.getString("student_id"));
			examDetailLookUpRsObject.setSubjectId(rs.getString("subject_id"));
			examDetailLookUpRsObject.setExamId(rs.getString("exam_id"));
			examDetailLookUpRsObject.setCourseId(rs.getString("course_id"));
			examDetailLookUpRsObject.setExamType(rs.getString("exam_type"));
			examDetailLookUpRsObject.setExamName(rs.getString("exam_name"));
			examDetailLookUpRsObject.setExamStd(rs.getString("exam_std"));
			examDetailLookUpRsObject.setExamCode(rs.getString("exam_code"));
			examDetailLookUpRsObject.setExamType(rs.getString("exam_year"));
			examDetailLookUpRsObject.setGrade(rs.getString("grade"));
			examDetailLookUpRsObject.setScore(rs.getInt("score"));
			examDetailLookUpRsObject.setResult(rs.getString("exam_result"));
		}else{
			examDetailLookUpRsObject.setStatus(false);
			examDetailLookUpRsObject.setMessage("Exam detail is not found for the given citizen!!!");
		}
	
		return examDetailLookUpRsObject;
		
	}

}