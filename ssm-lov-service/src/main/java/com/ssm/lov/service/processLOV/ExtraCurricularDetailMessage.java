package com.ssm.lov.service.processLOV;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ssm.datamodel.ExtraCurricularRequestObject;
/**
 * 
 * @author bujji
 *
 */
public class ExtraCurricularDetailMessage {
	private static Logger logger = LoggerFactory.getLogger(ExtraCurricularDetailMessage.class);
	private static final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
	
	public void processMessage(ExtraCurricularRequestObject bodyContent, Exchange exchange) {
		String lovId = "E" + dateFormat.format(LocalDateTime.now());
		exchange.getContext().getDataFormatResolver();
    }
}
