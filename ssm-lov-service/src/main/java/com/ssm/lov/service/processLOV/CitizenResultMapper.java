package com.ssm.lov.service.processLOV;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.ssm.datamodel.CitizenLookUpRsObject;

public class CitizenResultMapper implements RowMapper<CitizenLookUpRsObject> {

	@Override
	public CitizenLookUpRsObject mapRow(ResultSet rs, int idx) throws SQLException {
		CitizenLookUpRsObject citizenLookUpRsObject = new CitizenLookUpRsObject();
		if(rs != null){
			citizenLookUpRsObject.setStatus(true);
			citizenLookUpRsObject.setCitizenNo(rs.getInt("citizen_no"));
		}else{
			citizenLookUpRsObject.setStatus(false);
			citizenLookUpRsObject.setMessage("Ic number and citizen name is not matched with the availble data!");
		}
	
		return citizenLookUpRsObject;
		
	}

}