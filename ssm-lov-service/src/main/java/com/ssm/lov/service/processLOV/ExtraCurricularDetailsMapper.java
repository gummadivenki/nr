package com.ssm.lov.service.processLOV;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import com.ssm.datamodel.ExtraCurricularData;
import com.ssm.datamodel.ExtraCurricularResponseObject;

/**
 * 
 * @author bujji
 *
 */
public class ExtraCurricularDetailsMapper implements RowMapper<ExtraCurricularResponseObject>{

	private static Logger logger = LoggerFactory.getLogger(ExtraCurricularDetailsMapper.class);
	@Override
	public ExtraCurricularResponseObject mapRow(ResultSet rs, int arg1) throws SQLException {
		logger.info("Enter this block");
		ExtraCurricularResponseObject extraCircularDetails = new ExtraCurricularResponseObject();
		List<ExtraCurricularData> extraCurriculatList = new ArrayList<ExtraCurricularData>();
		if(rs != null){
			logger.info("ResultSet:::"+rs);
			extraCircularDetails.setStatus("SUCCESS");
			extraCircularDetails.setOperation("GET_CURRICULAR_DETAILS");
			ExtraCurricularData extraCurricularVo = new ExtraCurricularData();
			logger.info("Sponsor_ID::::"+(rs.getString("sponsor_id"))+"Sponsor_Email::::"+(rs.getString("sponsor_email")));
			extraCurricularVo.setSponsor_id((rs.getString("sponsor_id")));
			extraCurricularVo.setSponsor_email((rs.getString("sponsor_email")));
			extraCurricularVo.setSponsor_name((rs.getString("sponsor_name")));
			extraCurricularVo.setSponsor_phone_number((rs.getString("sponsor_phone_number")));
			extraCurricularVo.setSponsor_type_id((rs.getString("sponsor_type_id")));
			extraCurriculatList.add(extraCurricularVo);
			extraCircularDetails.setExtraCurricularData(extraCurriculatList);
		}else{
			extraCircularDetails.setStatus("FAIL-Data Can't Found......");
			extraCircularDetails.setOperation("GET_CURRICULAR_DETAILS");
			extraCircularDetails.setExtraCurricularData(extraCurriculatList);
		}
		
		
		return extraCircularDetails;
	}

	

}
