package com.mampu.ws;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class TestConnection {

	public static void main(String[] args) {
		
		Connection connection=null;
		try
		{
		    Class.forName("org.postgresql.Driver");
		    connection = DriverManager.getConnection("jdbc:postgresql://103.8.146.104:5432/NR", "postgres", "thepolice69");
		    System.out.println("Connection Status::"+connection);
		    
		    Statement stmt = null;
		    String query = "SELECT * FROM rp.citizen_reference";
		    
		    stmt = connection.createStatement();
	        ResultSet rs = stmt.executeQuery(query);
	        while (rs.next()) {
	            String ICNumber = rs.getString("IC_NO");
	            String Status = rs.getString("OKU_STATUS");
	            
	            System.out.println(ICNumber + "\t" + Status +
	                               "\t");
	        }
		    
		}
		catch(Exception e)
		{
		    System.out.println("Exception occurred is"+e.getMessage());            
		}
		
		

	}

}
