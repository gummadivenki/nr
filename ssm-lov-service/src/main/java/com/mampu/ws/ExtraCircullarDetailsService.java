package com.mampu.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.ssm.datamodel.ExtraCurricularRequestObject;
import com.ssm.datamodel.ExtraCurricularResponseObject;
/**
 * 
 * @author bujji
 *
 */
@WebService( name = "circullarWS")
public interface ExtraCircullarDetailsService {
	
	@WebResult(name = "extraCurricularRsObject")
	@WebMethod(operationName = "extraCurricularRqObject")
    public ExtraCurricularResponseObject extraCircullarDetails(
    		@WebParam(name = "extraCurricularRqObject")
    		ExtraCurricularRequestObject extraCircullarRequest);

}
