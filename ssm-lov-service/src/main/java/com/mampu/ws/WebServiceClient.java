package com.mampu.ws;
import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import com.ssm.datamodel.OKULookUpRqObject;
import com.ssm.ws.OKULookUpWS;

public class WebServiceClient {

	private static final String serviceURL = "http://10.24.18.122:8186/cxf/ssmWSOKULookup";
	public static void main(String[] args) {
		  QName serviceName = new QName("http://ws.ssm.com/", "OKULookUpWS");
	      URL wsdlURL;
		try {
			wsdlURL = new URL(serviceURL + "?wsdl");
			Service service = Service.create(wsdlURL, serviceName);
		      OKULookUpWS proxy = (OKULookUpWS)service.getPort(OKULookUpWS.class);
		      
		      // invoke methods
		      OKULookUpRqObject oku = new OKULookUpRqObject();
		      oku.setIcNo("931231-10-6351");
		      proxy.okulookupRequest(oku);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      

	}

}
