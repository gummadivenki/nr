/**
 * 
 */
package com.ssm.testng;

import java.io.StringWriter;
import java.net.URL;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * @author leoanbarasanm
 *
 */

public class CitizenLookupTestNGScript {

	private static Logger logger = LoggerFactory.getLogger(CitizenLookupTestNGScript.class);
	private static String webserviceUrl = "http://localhost:8080/cxf/ssmCitizenLookup?wsdl";
	private SOAPMessage createSOAPRequest(String icNo, String citizenName) {
		try {
			MessageFactory messageFactory = MessageFactory.newInstance();
	        SOAPMessage soapMessage = messageFactory.createMessage();
	        SOAPPart soapPart = soapMessage.getSOAPPart();
	        String serverURI = "http://ws.ssm.com/";
	        
	        // SOAP Envelope
	        SOAPEnvelope envelope = soapPart.getEnvelope();
	        envelope.addNamespaceDeclaration("ws", serverURI);
	        SOAPBody soapBody = envelope.getBody();
	        SOAPElement soapBodyElem = soapBody.addChildElement("citizenLookupRequest", "ws");
	        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("citizenLookUpRqObject");
	        SOAPElement soapBodyElem2 = soapBodyElem1.addChildElement("icNo");
	        soapBodyElem2.addTextNode(icNo);
	        SOAPElement soapBodyElem3 = soapBodyElem1.addChildElement("citizenName");
	        soapBodyElem3.addTextNode(citizenName);
	        
	        // SOAP Header
	        MimeHeaders headers = soapMessage.getMimeHeaders();
	        headers.addHeader("operationName", "citizenLookupRequest");
	        headers.addHeader("operationNamespace", serverURI);
	        soapMessage.saveChanges();
	        
	        return soapMessage;
		} catch (Exception e) {
			logger.info("Error at Soap Rquest ::: " + e.getMessage());
		}
		
		return null;
	}
	
	@Test (priority=1, description="Look for a citizen with valid ic number and citizen name.")
	public void testCitizenLookupWithValidDetail() {
		
		String response = null;
		boolean result = false;
		try {
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();
			URL url = new URL(webserviceUrl);
			SOAPMessage soapResponse = soapConnection.call(createSOAPRequest("671208000000", "NG LAY YONG"), url);
			
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            Source sourceContent = soapResponse.getSOAPPart().getContent();
            StringWriter outWriter = new StringWriter();
            StreamResult result2 = new StreamResult(outWriter);
            transformer.transform(sourceContent, result2);
            StringBuffer sb = outWriter.getBuffer(); 
            response = sb.toString();
            
	        soapConnection.close();
	        
	        // Need to change the code for comparison
	        if (response != null && response.contains("<status>true</status>") && response.contains("<citizenNo>877</citizenNo>")) {
				result = true;
			}
			
		} catch (UnsupportedOperationException e) {
			e.printStackTrace();
		} catch (SOAPException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Assert.assertEquals(result, true);
	}
	
	@Test (priority=2, description="Look for a citizen with invalid ic number and citizen name and expecting no match.")
	public void testCitizenLookupWithInValidDetail() {
		
		String response = null;
		boolean result = false;
		try {
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();
			URL url = new URL(webserviceUrl);
			SOAPMessage soapResponse = soapConnection.call(createSOAPRequest("6712080000", "NG LAY"), url);
			
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            Source sourceContent = soapResponse.getSOAPPart().getContent();
            StringWriter outWriter = new StringWriter();
            StreamResult result2 = new StreamResult(outWriter);
            transformer.transform(sourceContent, result2);
            StringBuffer sb = outWriter.getBuffer(); 
            response = sb.toString();
            
	        soapConnection.close();
	        
	        // Need to change the code for comparison
	        if (response != null && response.contains("<status>false</status>") && response.contains("<message>Ic number and citizen name is not matched with the availble data!!!</message>")) {
				result = true;
			}
			
		} catch (UnsupportedOperationException e) {
			e.printStackTrace();
		} catch (SOAPException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Assert.assertEquals(result, true);
	}
	
	@Test (priority=3, description="Look for a citizen with null value and expecting no match.")
	public void testCitizenLookupWithNullValue() {
		
		String response = null;
		boolean result = false;
		try {
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();
			URL url = new URL(webserviceUrl);
			SOAPMessage soapResponse = soapConnection.call(createSOAPRequest("NULL", "NULL"), url);
			
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            Source sourceContent = soapResponse.getSOAPPart().getContent();
            StringWriter outWriter = new StringWriter();
            StreamResult result2 = new StreamResult(outWriter);
            transformer.transform(sourceContent, result2);
            StringBuffer sb = outWriter.getBuffer(); 
            response = sb.toString();
            
	        soapConnection.close();
	        
	        // Need to change the code for comparison
	        if (response != null && response.contains("<status>false</status>") && response.contains("<message>Ic number and citizen name is not matched with the availble data!!!</message>")) {
				result = true;
			}
			
		} catch (UnsupportedOperationException e) {
			e.printStackTrace();
		} catch (SOAPException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Assert.assertEquals(result, true);
	}
}
