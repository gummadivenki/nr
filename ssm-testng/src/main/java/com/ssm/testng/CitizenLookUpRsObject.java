/**
 * 
 */
package com.ssm.testng;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author leoanbarasanm
 *
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="citizenLookUpRsObject")

public class CitizenLookUpRsObject {
	
	private Boolean status;
	
	private Integer citizenNo;
	
	private String message;

	/**
	 * @return the status
	 */
	public Boolean getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Boolean status) {
		this.status = status;
	}

	/**
	 * @return the citizenNo
	 */
	public Integer getCitizenNo() {
		return citizenNo;
	}

	/**
	 * @param citizenNo the citizenNo to set
	 */
	public void setCitizenNo(Integer citizenNo) {
		this.citizenNo = citizenNo;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
}
